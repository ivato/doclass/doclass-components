import React from 'react';
import ReactDOM from 'react-dom';
import { Vega } from 'react-vega';
import { renderComponent } from 'recompose';

const spec = {
  "width": 400,
  "height": 200,
  "data": [{ "name": "table" }],
  "signals": [
    {
      "name": "tooltip",
      "value": {},
      "on": [
        {"events": "rect:mouseover", "update": "datum"},
        {"events": "rect:mouseout",  "update": "{}"}
      ]
    }
  ],
  scales: [
    {
      name: 'xscale',
      type: 'band',
      domain: { data: 'table', field: 'x' },
      range: 'width',
    },
    {
      name: 'yscale',
      domain: { data: 'table', field: 'y' },
      nice: true,
      range: 'height',
    },
  ],

  axes: [{ orient: 'bottom', scale: 'xscale' }, { orient: 'left', scale: 'yscale' }],

  marks: [
    {
      type: 'rect',
      from: { data: 'table' },
      encode: {
        enter: {
          x: { scale: 'xscale', field: 'x', offset: 1 },
          width: { scale: 'xscale', band: 1, offset: -1 },
          y: { scale: 'yscale', field: 'y' },
          y2: { scale: 'yscale', value: 0 },
        },
        update: {
          fill: { value: 'steelblue' },
        },
        hover: {
          fill: { value: 'red' },
        },
      },
    },
    {
      type: 'text',
      encode: {
        enter: {
          align: { value: 'center' },
          baseline: { value: 'bottom' },
          fill: { value: '#333' },
        },
        update: {
          x: { scale: 'xscale', signal: 'tooltip.x', band: 0.5 },
          y: { scale: 'yscale', signal: 'tooltip.y', offset: -2 },
          text: { signal: 'tooltip.y' },
          fillOpacity: [{ test: 'datum === tooltip', value: 0 }, { value: 1 }],
        },
      },
    },
  ],
}

const barData = {
  table: [
    { x: 'A', y: 28 },
    { x: 'B', y: 55 },
    { x: 'C', y: 43 },
    { x: 'D', y: 91 },
    { x: 'E', y: 81 },
    { x: 'F', y: 53 },
    { x: 'G', y: 19 },
    { x: 'H', y: 87 },
  ]
};

function handleHover(...args){
  console.log(args);
}

const signalListeners = { hover: handleHover };

function BarChart(props) {
  return(
    <Vega 
      spec={spec} 
      data={barData} 
      signalListeners={signalListeners} 
    />
  )  
}

export default BarChart;
