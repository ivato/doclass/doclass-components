import React from 'react'
// import { useUID, useUIDSeed } from 'react-uid'
import { UIDConsumer, UIDFork, useUIDSeed } from 'react-uid'

const components = [
  { name: 'one' },
  { name: 'two' },
  { name: 'three' }
]

function UIDTests(props) {
  const getId = useUIDSeed()
  return (
    <React.Fragment>
      <UIDConsumer>
        { (id,uid) => (
          <>
            <p>{uid(components[0])}</p>
            <p>{uid(components[1])}</p>
          </>
        )
        }
      </UIDConsumer>
      <UIDConsumer>
        { (id,uid) => (
          <>
            <p>{uid(components[0])}</p>
            <p>{uid(components[1])}</p>
            <p>{uid(components[2])}</p>
          </>
        )
        }
      </UIDConsumer>
      <UIDConsumer>
        { (id,uid) => (
          <>
            <p>{uid(components[0])}, {id}</p>
            <p>{uid(components[1])}, {id}</p>
            <p>{uid(components[2])}, {id}</p>
          </>
        )
        }
      </UIDConsumer>
      <UIDFork>
        <UIDConsumer>
          { (id,uid) => (
            <>
              <p>{uid(components[0])}, {id}</p>
              <p>{uid(components[1])}, {id}</p>
              <p>{uid(components[2])}, {id}</p>
            </>
          )
          }
        </UIDConsumer>
      </UIDFork>
      <UIDFork>
        <p>{getId(components[0])}</p>
        <p>{getId(components[1])}</p>
        <p>{getId(components[2])}</p>
      </UIDFork>
    </React.Fragment>
  )
}

export default UIDTests