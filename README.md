# Doclass components

Web components to operate doclass backend

## Prerequisites
* nodejs >= 12
* yarn >= 1.19

## Instalation

1. Clone the project

```bash
git clone https://gitlab.com/ivato/textgraph/doclass-components
```

2. Install javascript libraries

It may take a long time ...

```bash
cd doclass-components
yarn
```

3. Configuration

Open src/config.js file and change config.endpoint.base key, default value is 'http://localhost:5000'.

4. Browse and test components

We use the Storybook to view and test components.

```bash
yarn storybook
```

Open your browser at http://localhost:9009/ - you can change default port at package.json file.