import { createMuiTheme } from '@material-ui/core/styles'

export default function styles () {
  const theme = createMuiTheme({
    spacing: 8
  })

  const styles = {
    root: {
      display: 'flex',
      justifyContent: 'left',
      flexWrap: 'wrap',
      padding: theme.spacing(0.5)
    },
    chip: {
      margin: theme.spacing(0.5)
    }
  }

  return styles
}
