import React, {useEffect} from 'react'
import SelectItemContainer from '../containers/SelectItemContainer'
import { 
  useStoreState,
  useStoreActions 
} from 'easy-peasy'
import { useUID } from 'react-uid'
import config from '../config'

const LabelsetSelect = function (props) {
  const {
    cbChange,
    excludedValues,
    excludedField,
    parent
  } = props

  const name = 'LabelsetSelect'  

  const registerComponent = useStoreActions(actions => actions.registerComponent);
  const unRegisterComponent = useStoreActions(actions => actions.unRegisterComponent);
  const uid = props.uid ? props.uid : '1-'+name
  const uid_prefix = uid.substring(0,uid.lastIndexOf('-'))  
  useEffect(()=>{
    registerComponent({ uid, parent, name })
    return () => { unRegisterComponent({uid,name}) }
  },[]) 

  const sicuid = uid_prefix+'.1-SelectItemContainer' 
  const endpoint = config.getEndpoint(
    'labelset',
    'get_all',
    {}
  )

  return (
    <SelectItemContainer
      key_='_id'
      title='Labelset'
      showid='false'
      endpoint={endpoint}
      field='full_name'
      cbChange={cbChange}
      excludedValues={excludedValues}
      excludedField={excludedField}
      parent={uid}
      uid={sicuid}
    />
  )
}

export default LabelsetSelect
