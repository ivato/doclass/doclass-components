import React, { useEffect, useState } from 'react'
import SelectItemContainer from '../containers/SelectItemContainer'
import LabelPanelContainer from '../containers/LabelPanelContainer'
import {
  useStoreActions
} from 'easy-peasy'
import { useUID } from 'react-uid'
import config from '../config'

export function DocumentSelect(props) {

  const name = 'DocumentSelect'
  const { parent } = props
  const registerComponent = useStoreActions(actions => actions.registerComponent)
  const unRegisterComponent = useStoreActions(actions => actions.unRegisterComponent)
  const uid = props.uid ? props.uid : '1-'+name
  const uid_prefix = uid.substring(0,uid.lastIndexOf('-'))
  const sicuid = uid_prefix+'.1-SelectItemContainer' 

  useEffect(()=>{
    registerComponent({ uid, parent, name })
    return () => { unRegisterComponent({ uid, name }) }
  },[])

  const endpoint =  props.endpoint ? 
                    props.endpoint : 
                    config.getEndpoint('document', 'get_all', {})

  return (
    <SelectItemContainer
      key_='_id'
      title='Document'
      showid='false'
      endpoint={endpoint}
      field='filename'
      model='document'
      parent={uid}
      uid={sicuid}
    />
  )
}

export function DocumentLabelPanel(props) {
  const {
    title,
    deletable,
    options,
    parent
  } = props

  const name = 'DocumentLabelPanel'
  const registerComponent = useStoreActions(actions => actions.registerComponent)
  const unRegisterComponent = useStoreActions(actions => actions.unRegisterComponent)
  const uid = props.uid ? props.uid : '1-'+name
  const uid_prefix = uid.substring(0,uid.lastIndexOf('-'))  
  const lpcuid = uid_prefix+'.1-LabelPanelContainer'

  useEffect(()=>{
    registerComponent({ uid, parent, name })
    return () => { unRegisterComponent({uid,name}) }
  },[])

  const [ endpoint, setEndpoint ] = useState('')

  useEffect(()=>{
    if ( props.endpoint )
      setEndpoint(props.endpoint)
  },[])
  
  return (
    <LabelPanelContainer
      title={title}
      clickable={false}
      deletable={deletable}
      endpoint={endpoint}
      options={options}
      parent={uid}
      uid={lpcuid}
    />
  )
}