import React, { useEffect } from 'react'
import SelectItemContainer from '../containers/SelectItemContainer'
import { useStoreActions } from 'easy-peasy'
import config from '../config'

function RepositorySelect (props) {
  const endpoint =  props.endpoint ? 
                    props.endpoint :
                    config.getEndpoint('repository', 'get_all', {}) 

  const name = 'RepositorySelect'
  const { parent } = props
  const registerComponent = useStoreActions(actions => actions.registerComponent);
  const unRegisterComponent = useStoreActions(actions => actions.unRegisterComponent);

  const uid = props.uid ? props.uid : '1-'+name
  const uid_prefix = uid.substring(0,uid.lastIndexOf('-'))
  const sicuid = uid_prefix+'.1-SelectItemContainer' 

  useEffect(() => {
    registerComponent({ uid, parent, name })
    return () => { unRegisterComponent({uid,name}) }
  },[])
                  
  return (    
    <SelectItemContainer
      key_='_id'
      title='Repository'
      showid='false'
      endpoint={endpoint}
      field='relative_path'
      parent={uid}
      uid={sicuid}
    />
  )
}

export default RepositorySelect
