import React, { useEffect } from 'react'
import SelectItemContainer from '../containers/SelectItemContainer'
import { useUID } from 'react-uid' 
import { useStoreActions } from 'easy-peasy'
import config from '../config'

function CollectionSelect (props) {
  const name = 'CollectionSelect'
  const { parent } = props
  const registerComponent = useStoreActions(actions => actions.registerComponent)
  const unRegisterComponent = useStoreActions(actions => actions.unRegisterComponent)

  const uid = props.uid ? props.uid : '1-'+name
  const uid_prefix = uid.substring(0,uid.lastIndexOf('-'))
  const sicuid = uid_prefix+'.1-SelectItemContainer'
  const endpoint = config.getEndpoint('collection', 'get_all', {})

  useEffect(()=>{
    registerComponent({ uid, parent, name })
    return () => { unRegisterComponent({uid,name}) }
  },[])
  
  return (
    <SelectItemContainer
      key_='_id'
      title='Collection'
      showid='false'
      endpoint={endpoint}
      field='name'
      parent={uid}
      uid={sicuid}
    />
  )
}

export default CollectionSelect
