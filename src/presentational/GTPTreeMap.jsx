import React, {useEffect} from "react";
import { Chart } from "react-google-charts";
import { 
  useStoreState,
  useStoreActions 
} from 'easy-peasy'
 
export function GTPTreeMap(props) {
  const name = 'GTPTreeMap'  
  const { 
    parent, 
    data
  } = props
  const { 
    yh_yc, nh_nc, uh_uc, 
    yh_nc, yh_uc, 
    nh_yc, nh_uc, 
    uh_yc, uh_nc
  } = data 
  const registerComponent = useStoreActions(actions => actions.registerComponent);
  const unRegisterComponent = useStoreActions(actions => actions.unRegisterComponent)
  const uid = props.uid ? props.uid : '1-'+name
  const uid_prefix = uid.substring(0,uid.lastIndexOf('-'))

  useEffect(()=>{
    registerComponent({ uid, parent, name })
    return () => { unRegisterComponent({ uid, name }) }
  },[]) 

  return (
    <div className={"my-pretty-chart-container"}>
      <Chart
        width={'500px'}
        height={'300px'}
        chartType="TreeMap"
        loader={<div>Loading Chart</div>}
        data={[
          [
            'GTP',
            'Parent',
            'Quantity',
            'Absolute difference of value',
          ],
          ['Ground Truth / Prediction', null, 0, 0],
          ['YH/YC', 'Ground Truth / Prediction', yh_yc, 0],
          ['NH/NC', 'Ground Truth / Prediction', nh_nc, 0],
          ['UH/UC', 'Ground Truth / Prediction', uh_uc, 0],
          ['YH/NC', 'Ground Truth / Prediction', yh_nc , 2],
          ['YH/UC', 'Ground Truth / Prediction', yh_uc, 0.5],
          ['NH/YC', 'Ground Truth / Prediction', nh_yc, 2],
          ['NH/UC', 'Ground Truth / Prediction', nh_uc, 0.5],
          ['UH/YC', 'Ground Truth / Prediction', uh_yc, 0.5],
          ['UH/NC', 'Ground Truth / Prediction', uh_nc, 0.5],
        ]}
        options={{
          minColor: '#6600ff',
          midColor: '#cc33ff',
          maxColor: '#cc3399',
          headerHeight: 15,
          fontColor: 'black',
          showScale: true,
        }}
        rootProps={{ 'data-testid': '1' }}
      />
    </div>    
    );
}

export default GTPTreeMap