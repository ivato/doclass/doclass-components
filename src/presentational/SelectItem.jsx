import React, { useEffect } from 'react'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import InputLabel from '@material-ui/core/InputLabel'
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { makeStyles } from '@material-ui/core/styles';
import ReactSelect from '../presentational/ReactSelect'
import { 
  useStoreState,
  useStoreActions 
} from 'easy-peasy'

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    height: 50,
    maxWidth: 80,
    backgroundColor: theme.palette.background.paper,
  },
}));

function SelectItem (props) {
  const name = 'SelectItem'
  const {
    items,
    key_,
    showid,
    field,
    title,
    parent
  } = props

  const updateSelected = useStoreActions(actions => actions.updateSelected)  
  const registerComponent = useStoreActions(actions => actions.registerComponent)
  const clearEvent = useStoreActions(actions => actions.clearEvent)
  const unRegisterComponent = useStoreActions(actions => actions.unRegisterComponent)
  const uid = props.uid ? props.uid : '1-'+name
  const uid_prefix = uid.substring(0,uid.lastIndexOf('-'))
  const rsuid = uid_prefix+'.1-'+name

  useEffect(() => {
    registerComponent({ uid, parent, name })
    return () => { 
      unRegisterComponent({uid, name}) 
      clearEvent({uid, name: 'selected'})
    }
  }, [])

  const handleChange = (e) => {
    updateSelected({ name, uid, data: e.value })    
  }

  const suggestions =  items.map(item => ({
    value: item[key_],
    label: item[field],
    key: item[key_]
  }));    

  const classes = useStyles();
  return (
    <ReactSelect
        title={title}
        suggestions={suggestions}
        isMulti={false}
        placeholder='type...'
        onChange={handleChange}
        parent={uid}
        uid={rsuid}
    />
    )
}

export default SelectItem