import React, {useState} from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
// import ListItemText from '@material-ui/core/ListItemText';
import { FixedSizeList } from 'react-window';
import Select from '@material-ui/core/Select'

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    height: 400,
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

function Row(props) {
  const { index, style } = props;

  return (
    <MenuItem style={style} key={index}>
      {`Item ${index + 1}`}
    </MenuItem>
  );
  /* let selected = false
  if ( index === 1)
    selected = true*/
  /* return (
    <option value={index} style={style} key={index}>
      {`Item ${index + 1}`}
    </option>
  )*/
}


Row.propTypes = {
  index: PropTypes.number.isRequired,
  style: PropTypes.object.isRequired,
};

export default function VirtualizedList() {
  const classes = useStyles();
  const [selected, setSelected] = useState('')
  const props = {}
  const handleChange = (e) => {
    console.log('handleSelect')
    setSelected(e.target.value)
  }
  
  return (
    <div className={classes.root}>      
      <Select 
        props={ 
          { onChange: handleChange }
        }
        value={selected}
        // onChange={handleChange}
        // defaultValue={'2'}
        // native={true}
      >
        <FixedSizeList 
          height={400} 
          width={360} 
          itemSize={46} 
          itemCount={200}
          onChange={handleChange} 
          {...props}>
          {Row}
        </FixedSizeList>
      </Select>
    </div>
  );
}
