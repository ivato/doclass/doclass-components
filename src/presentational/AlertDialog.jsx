import React, {useEffect} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { 
  useStoreState,
  useStoreActions 
} from 'easy-peasy'

export default function AlertDialog(props) {
  const name = 'AlertDialog'  
  const { 
    parent,
    title,
    content
   } = props
  const registerComponent = useStoreActions(actions => actions.registerComponent)
  const unRegisterComponent = useStoreActions(actions => actions.unRegisterComponent)
  const uid = props.uid ? props.uid : '1-'+name
  const uid_prefix = uid.substring(0,uid.lastIndexOf('-'))

  useEffect(()=>{
    registerComponent({ uid, parent, name })
    return () => { unRegisterComponent({ uid, name }) }
  },[]) 

  const dialog = useStoreState(state => state.events.dialog)
  const updateDialog = useStoreActions(actions => actions.updateDialog)

  function handleClose() {
    updateDialog({isopen: false, uid});
  }

  return (
    <div>
      <Dialog
        open={dialog[uid]}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        title={title}
        content={content}
      >
        <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {content}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            OK
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
