import React, { Component } from "react";
// import ReactDOM from "react-dom";
import Select from "react-select";
// import Select from '@material-ui/core/Select'
import { FixedSizeList as List } from "react-window";

import "./styles.css";

const options = [];
for (let i = 0; i < 10000; i = i + 1) {
  options.push({ value: i, label: `Option ${i}` });
}

const height = 35;

class MenuList extends Component {
  render() {
    const { options, children, maxHeight, getValue } = this.props;
    const [value] = getValue();
    const initialOffset = options.indexOf(value) * height;

    return (
      <List
        height={maxHeight}
        itemCount={children.length}
        itemSize={height}
        initialScrollOffset={initialOffset}
      >
        {({ index, style }) => <div style={style}>{children[index]}</div>}
      </List>
    );
  }
}

function SelectWindow(props) {
  return(
    <Select 
      onChange={console.log('changed')}
      components={{ MenuList }} 
      options={options} />
  )
}

export default SelectWindow

// const App = () => <Select components={{ MenuList }} options={options} />;

// ReactDOM.render(<App />, document.getElementById("root"));
