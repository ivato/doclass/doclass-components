import React, {useEffect, useState} from 'react'
import Chip from '@material-ui/core/Chip'
import Tooltip from '@material-ui/core/Tooltip'
import Paper from '@material-ui/core/Paper'
import styles from '../styles/styles'
import { withStyles } from '@material-ui/core/styles'
import { 
  useStoreState,
  useStoreActions 
} from 'easy-peasy'
import { useUID } from 'react-uid'

function LabelPanel (props) {
  const name = 'LabelPanel'  
  const { parent } = props
  const registerComponent = useStoreActions(actions => actions.registerComponent)
  const unRegisterComponent = useStoreActions(actions => actions.unRegisterComponent)
  const uid = props.uid ? props.uid : '1-'+name

  useEffect(()=>{
    registerComponent({ uid, parent, name })
    return () => { unRegisterComponent({uid, name}) }
  },[]) 

  const [hiddenLabelsID, setHiddenLabelsID] = useState([])

  let labelsSet = []

  const {
    labels,
    clickable,
    deletable,
    options,
    classes,
    title
  } = props

  const updateClickedLabels = useStoreActions(actions => actions.updateClickedLabels)
  const handleClick = (e, data) => {
    updateClickedLabels({uid, data})
  }

  const updateDeletedLabels = useStoreActions(actions => actions.updateDeletedLabels)
  const handleDelete = (e, data) => {
    updateDeletedLabels({uid, data})
  }

  function nameFilter (options, data) {
    const newName = 
      (options && options.alias && options.alias[name]) ? 
      options.alias[name] : 
      (data.alias) ? data.alias :
      data.full_name
    return newName
  }

  function nameColor (options, data) {
    const color = 
      (options && options.color && options.color[name]) ? 
      options.color[name] : 
      (data.color) ? data.color :
      '#CCCCCC'
    return color
  }

  function deletableFilter (options, deletable, name ) {
    if ( options && options.deletable ) {
      const reDelOptFalse = new RegExp(options.deletable['false'])
      if ( reDelOptFalse.test(name) )
        return 'false'
    }
    return deletable
  }

  const hidden = useStoreState(state => state.events.hidden_labels)
  useEffect(() => {
    let hl_ids = [] 
    if ( typeof hidden[uid] !== 'undefined') {
      hl_ids = hidden[uid].map( (l) => { return l._id })
      setHiddenLabelsID(hl_ids)
    }  
  },[hidden[uid]])

  if (typeof labels !== 'undefined' && Array.isArray(labels) && Object.keys(labels).length) {
    const filtered_labels = labels.filter((label) => (! hiddenLabelsID.includes(label._id)))
    labelsSet = filtered_labels.map(data => {
      var hc = null
      if (clickable) 
        { hc = (e) => { handleClick(e, data ) } }
      var hd = null
      if ( deletableFilter(options, deletable, data.full_name) === 'true')  
        { hd = (e) => handleDelete(e, data ) }
      return (
          <Tooltip
            title={data.description}
            key={data._id.toString()}
          >
          <Chip className={classes.chip}
            key={data._id.toString()}
            label={
              nameFilter(options, data)
            }
            onClick={hc}
            onDelete={hd}
            color='primary'
            style={{
              color: 'gray',
              backgroundColor: nameColor(options, data)
            }}
            clickable={clickable}
          />
        </Tooltip>
      )
    })
  }
  return (
    <Paper className={classes.root} >
      <span>{title} </span>
      {labelsSet}
    </Paper>
  )
}
export default withStyles(styles())(LabelPanel)
