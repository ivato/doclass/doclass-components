import React from 'react'
import TextAreaMUI from 'mineral-ui/TextArea'
import { 
  useStoreState,
  useStoreActions 
} from 'easy-peasy'

function TextArea (props) {

  const { 
    parent,
    content = ''
  } = props
  const name = 'TextArea'
  const registerComponent = useStoreActions(actions => actions.registerComponent)
  const unRegisterComponent = useStoreActions(actions => actions.unRegisterComponent)
  const uid = props.uid ? props.uid : '1-'+name
  const uid_prefix = uid.substring(0,uid.lastIndexOf('-'))

  React.useEffect(()=>{
    registerComponent({ uid, parent, name })
    return () => { 
      unRegisterComponent({uid,name}) 
    }
  },[]) 

  return (
    <TextAreaMUI
      rows={50}
      readOnly
      value={ content }
      style={{ overflowY: 'auto' }}
    />
  )
}

export default TextArea
