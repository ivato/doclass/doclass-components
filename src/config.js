export const config = {
  endpoint: {	  
    base: 'http://localhost:5000',
    models: {
      document: {
        base: '/document',
        operations: {
          get_all: (args) => ``,
          get_labels: (args) => `/${args['doc_id']}/labels`,
          apply_label:(args) => `/${args['doc_id']}/add/${args['label_id']}`,
          remove_label: (args) => `/${args['doc_id']}/remove/${args['label_id']}`,
          get_content:(args) => `/content/${args['doc_id']}`,
          docs_repo: (args) => `/repo/${args['repo_id']}`,
          get_all_classifications: (args) => `/classification`,
          get_classifications: (args) => `/classification/${args['entity']}/${args['id']}`,
        }
      },
      collection: {
        base: '/collection',
        operations: {
          get_all: (args) => ``,
          repo_list: (args) => `/${args['collection_id']}/repo_list`,
        }
      },
      repository: {
        base: '/repo',
        operations: {
          get_all: (args) => ``,
        }
      },
      labelset: {
        base: '/labelset',
        operations: {
          get_all: (args) => ``,
          get_labels: (args) => `/${args['labelset_id']}/labels`,
        }
      }
    }
  },
  getEndpoint: (model, operation, args) => {
    const base = config.endpoint['base'] 
    const model_base = config.endpoint.models[model]['base'] 
    const template = config.endpoint.models[model]['operations'][operation]
    return base+model_base+template(args)
  }
}

export default config
