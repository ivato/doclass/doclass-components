import { thunk, action, actionOn } from 'easy-peasy'
import { filter, intersection, map } from 'lodash'
import axios from 'axios'

import config from './config'

const component_events = {
  'SelectItem': { selected: '' },
  'SelectItemContainer': { items: [] },
  'RepositorySelect': { endpoint: '' },
  'CollectionSelect': { endpoint: '' },
  'DocumentSelect': { endpoint: '' },
  'LabelsetSelect': { endpoint: '' },
  'SelectDocumentContainer': {},
  'LabelPanelContainer': { labels: {}, endpoint: ''},
  'LabelPanel': { clicked_labels: {}, selected_labels: {}, deleted_labels: {} },
  'LabelsetPanelContainer': {},
  'DocViewerContainer': { dialog: false  },
  'DocumentLabelPanel': { document_id: '' },
  'TextArea': { contents: '' },
  'AlertDialog': { dialog: false },
  'DocBrowserContainer': {},
  'GTPTreeMap': {},
  'DocExplorerContainer': {}
}  
export const storeModel = {
  // events
  events: {
    selected: {},
    endpoint: {},
    labels: {},
    hidden_labels: {},
    clicked_labels: {},
    selected_labels: {},
    deleted_labels: {},
    dialog: {},
    contents: {},
    items: {},
    document_id: {}
  },
  clearEvent: action((state,payload) => {
    const {
      uid,
      name,
    } = payload
    if ( state.events[name][uid] ) {
      delete state.events[name][uid]
    }
  }),
  // components
  components: { count: 0 },
  registerComponent: action((state,payload) => {
    const {
      uid,
      parent,
      name
    } = payload
    state.components[uid] = { parent, name }
    Object.keys(component_events[name]).forEach( 
      (event) => { 
        state.events[event][uid] = component_events[name][event] 
    })
    state.components.count++
  }),
  unRegisterComponent: action((state,payload) => {
    const {
      uid,
      name
    } = payload
    if ( delete state.components[uid] )
      state.components.count--
    Object.keys(component_events[name]).forEach( 
      (event) => { 
        delete state.events[event][uid] 
    })  
  }),
  // items
  updateItems: thunk(async(actions, payload) => {
    const {
      endpoint,
      excludedValues,
      excludedField,
      uid,
    } = payload
    const { data } = await axios(endpoint)
    let fIds = []
    
    if (typeof excludedValues !== 'undefined' &&
              typeof excludedField !== 'undefined'
    ) {
      const ev = excludedValues
      const ef = excludedField
      const f = filter(data,
        (e) => {
          if (intersection(ev, e[ef]).length !== 0) {
            return e._id
          }
        }
      )
      fIds = map(f, '_id')
    }
    const d = data
    const items = d.filter((d) => (!fIds.includes(d._id)))      
    actions.setItems({ uid, data: items })
  }),
  setItems: action((state,payload) => {
    const { uid, data } = payload
    state.events['items'][uid] = data
  }),
  updateSelected: action((state,payload) => {
    const { uid, data } = payload
    state.events['selected'][uid] = data 
  }),
  // endoint
  setEndpoint: action((state,payload) => {
    const {
      uid,
      endpoint,
    } = payload
    state.events['endpoint'][uid] = endpoint
  }),
  // labels
  updateLabels: thunk(async(actions, payload) => {
    const {
      endpoint,
      uid
    } = payload
    if ( endpoint ) {
      const { data } = await axios(endpoint)
      actions.setLabels({uid, data})        
    }
  }),
  setLabels: action((state,payload) => {
    const {uid, data} = payload
    state.events['labels'][uid] = data
  }),
  updateDocumentLabels: thunk(async(actions, payload) => {
    const {
      doc_id,
      uid,
      hidden_uid,
    } = payload
    if ( doc_id ) {
      const endpoint = config.getEndpoint('document', 'get_labels',{ doc_id })
      const { data } = await axios(endpoint)      
      actions.setLabels({uid,data})
      const origin_uid = uid
      if ( hidden_uid ) {
        actions.updateHiddenLabels({origin_uid,hidden_uid})
      }
    }
  }),
  updateHiddenLabels: action((state, payload) => {
    const { origin_uid, hidden_uid } = payload
    const hidden_labels = state.events['labels'][origin_uid]
    state.events['hidden_labels'][hidden_uid] = hidden_labels
  }),
  updateClickedLabels: action((state, payload) => {
    const { uid, data } = payload
    state.events['clicked_labels'][uid] = data
  }),
  applyLabel: thunk(async(actions,payload) => {
    const { origin_uid, label_id, target_uid, doc_id, alert_uid } = payload
    const endpoint = config.getEndpoint('document', 'apply_label', {doc_id, label_id})
    try {
      const result = await axios(endpoint)
      actions.updateDocumentLabels(
        {
          doc_id, 
          uid: target_uid,
          hidden_uid: origin_uid,
        })  
    } catch (e) {
      const isopen = true
      const uid = alert_uid
      actions.updateDialog({isopen, uid})
    }
  }),
  removeLabel: thunk(async(actions,payload) => {
    const {doc_id, uid, label_id, hidden_uid} = payload
    const endpoint = config.getEndpoint('document', 'remove_label', {doc_id, label_id})
    const result = await axios(endpoint)
    actions.updateDocumentLabels({ doc_id, uid, hidden_uid })
  }),
  updateDeletedLabels: action((state, payload) => {
    const { uid, data } = payload
    state.events['deleted_labels'][uid] = data
  }),
  // dialog
  updateDialog: action((state,payload) => {
    const {
      isopen,
      uid
    } = payload
    state.events['dialog'][uid] = isopen
  }),
  // content
  updateContent: thunk(async(actions, payload) => {
    const {
      doc_id,
      uid
    } = payload
    if ( doc_id ) {
      const endpoint = config.getEndpoint('document', 'get_content', {doc_id})
      const { data } = await axios(endpoint)
      actions.setContent({uid, data})
    }
  }),
  setContent: action((state,payload) => {
    const {uid, data} = payload
    state.events['contents'][uid] = data
  }),
  // document
  updateDocumentId: action((state,payload) => {
    const { doc_id, uid } = payload
    state.events['document_id'][uid] = doc_id
  })
}

export default storeModel