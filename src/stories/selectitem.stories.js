import React from 'react'

import { storiesOf } from '@storybook/react'

import SelectItemContainer from '../containers/SelectItemContainer'
import SelectItem from '../presentational/SelectItem'
import RepositorySelect from '../models/repository'
import CollectionSelect from '../models/collection'
import { DocumentSelect } from '../models/document'
import LabelsetSelect from '../models/labelset'
import SelectDocumentContainer from '../containers/SelectDocumentContainer'
import VirtualizedList from '../presentational/VirtualizedList'
import SelectWindow from '../presentational/SelectWindow'
import ReactSelect from '../presentational/ReactSelect'

import { 
  createStore, 
  StoreProvider,   
} from 'easy-peasy'
import { storeModel } from '../store'
const store = createStore(
  storeModel,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

storiesOf('SelectItem', module)
.addDecorator(
    (getStory) => (<StoreProvider store={store}>
      {getStory()}
    </StoreProvider>
    )
  )
.add('Stateless, without connection', () =>
        <SelectItem
            title='Stateless'
            key_='_id'
            field='full_name'
            items={[ {_id: 1, full_name: 'one'}, {_id: 2, full_name: 'two'}]}
            selected_value=''
        />
    )

storiesOf('SelectItemContainer', module)
  .addDecorator(
    (getStory) => (<StoreProvider store={store}>
      {getStory()}
    </StoreProvider>
    )
  )
  .add('all collections with id', () =>
    <SelectItemContainer
      title='Collection'
      endpoint='http://localhost:5000/collection'
      key_='_id'
      field='name'
      showid='true'
      model='collection'
    />)
  .add('all repositories', () =>
    <SelectItemContainer
      key_='_id'
      title='Repository'
      showid='false'
      endpoint='http://localhost:5000/repo'
      field='relative_path'
      model='repository'
    />
  )
  .add('list documents 100 documents from 10th document', () =>
    <SelectItemContainer
      title='Document'
      endpoint='http://localhost:5000/document/list/10/100'
      key_='_id'
      field='filename'
      model='document'
    />)
  .add('all labelsets', () =>
    <SelectItemContainer
      title='Labelset'
      endpoint='http://localhost:5000/labelset'
      key_='_id'
      model='labelset'
      field='full_name'
    />)
  .add('all labels', () =>
    <SelectItemContainer
      title='Label'
      endpoint='http://localhost:5000/label'
      key_='_id'
      field='full_name'
      model='label'
    />)

 storiesOf('Model Components', module)
  .addDecorator(
    (getStory) => (<StoreProvider store={store}>
      {getStory()}
    </StoreProvider>
    )
  )
  .add('RepositorySelect', () =>
    <RepositorySelect />)
  .add('CollectionSelect', () =>
    <CollectionSelect />)
  .add('DocumentSelect', () =>
    <DocumentSelect />)
  .add('LabelsetSelect', () =>
    <LabelsetSelect />)     

storiesOf('Select Document Container', module)
  .addDecorator(
    (getStory) => (<StoreProvider store={store}>
      {getStory()}
    </StoreProvider>
    )
  )
  .add('SelectDocumentContainer', () =>
    <SelectDocumentContainer />)

/* storiesOf('Virtualized List', module)
    .add('VirtualizedList', () =>
      <VirtualizedList />)

storiesOf('Select Window', module)
    .add('SelectWindow', () =>
      <SelectWindow />) */