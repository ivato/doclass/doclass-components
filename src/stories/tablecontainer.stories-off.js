import React from 'react'

import { storiesOf } from '@storybook/react'
import TableContainer from '../containers/TableContainer'

function renderLink (id, ep) {
  return (
    <spam>
      <a href={ep + '/' + id}>{id}</a>&nbsp;
    </spam>
  )
}

function renderFileLink (id, fn, ep) {
  return (<spam><a href={ep + '/' + id}>{fn}</a>  </spam>)
}

function renderList (item, index) {
  if (index === 0) {
    return (<spam>{item}</spam>)
  } else {
    return (<spam>, {item}</spam>)
  }
}
storiesOf('TableContainer', module)
  .add('repos', () => <TableContainer
    editableOption='false'
    endpoint='http://doclass.localhost/repo/'
    title='Repositories table'
    entity='repository'
    columns={[
      {
        field: '_id',
        title: 'Id',
        editable: 'never'
      },
      {
        title: 'Relative path',
        field: 'relative_path'
      }
    ]}
  />)
  .add('collections', () => <TableContainer
    endpoint='http://doclass.localhost/collection/'
    title='Collections table'
    entity='collection'
    columns={[
      {
        field: '_id',
        title: 'Id',
        editable: 'never'
      },
      {
        field: 'name',
        title: 'Name'
      },
      {
        field: 'description',
        title: 'Description'
      },
      {
        field: 'repo_list',
        title: 'List of Repositories',
        render: rowData => rowData.repo_list.map(renderLink, 'http://doclass.localhost/collection/')
      }
    ]}
  />
  )
  .add('labelset', () => <TableContainer
    endpoint='http://doclass.localhost/labelset/'
    title='Labelsets table'
    entity='labelset'
    columns={[
      {
        field: '_id',
        title: 'Id',
        editable: 'never'
      },
      {
        field: 'name',
        title: 'Name'
      },
      {
        field: 'full_name',
        title: 'Full Name'
      },
      {
        field: 'description',
        title: 'Description'
      },
      {
        field: 'parent',
        title: 'Parent'
      },
      {
        field: 'property',
        title: 'Property'
      },
      {
        field: 'properties',
        title: 'Properties',
        editable: 'never',
        render: rowData => rowData.properties.map(renderList)
      },
      {
        field: 'labels',
        editable: 'never',
        title: 'List of Labels',
        render: rowData => rowData.labels.map(renderLink, 'http://doclass.localhost/label/')
      }
    ]}
  />
  )
  .add('label', () => <TableContainer
    endpoint='http://doclass.localhost/label/'
    title='Labels table'
    entity='label'
    columns={[
      {
        field: '_id',
        title: 'Id',
        editable: 'never'
      },
      {
        field: 'name',
        title: 'Name'
      },
      {
        field: 'full_name',
        title: 'Full Name'
      },
      {
        field: 'description',
        title: 'Description'
      },
      {
        field: 'parent',
        title: 'Parent'
      },
      {
        field: 'property',
        title: 'Property'
      },
      {
        field: 'label_set',
        title: 'Labelset'
      }
    ]}
  />
  )
  .add('docs', () => <TableContainer
    endpoint='http://doclass.localhost/document/'
    title='Documents table'
    entity='document'
    columns={[
      {
        field: '_id',
        title: 'Id',
        editable: 'never'
      },
      {
        field: 'filename',
        title: 'Filename',
        editable: 'never',
        render: rowData => renderFileLink(rowData._id, rowData.filename, 'http://doclass.localhost/document/')
      },
      {
        field: 'repo',
        title: 'Repository',
        editable: 'never'
      },
      {
        field: 'labels',
        title: 'Labels',
        editable: 'never',
        render: rowData => rowData.labels.map(renderLink, 'http://doclass.localhost/label/')
      }
    ]}
  />
  )
