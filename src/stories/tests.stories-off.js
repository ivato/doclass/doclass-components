import React from 'react'
import { storiesOf } from '@storybook/react'

import UIDTests from '../../tests/UIDTests'
import { UIDReset } from 'react-uid'
import BarChart from '../../tests/BarChart'
import TreeMap from '../../tests/TreeMap'

import { 
  createStore, 
  StoreProvider,   
} from 'easy-peasy'
import { storeModel } from '../store'
const store = createStore(
  storeModel,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

storiesOf('vega', module)
  .addDecorator(
    (getStory) => (<StoreProvider store={store}>
      {getStory()}
    </StoreProvider>
    )
  )
  .add('bar chart', () =>
    <BarChart/>)
  .add('treemap', () =>
    <TreeMap/>)

/* storiesOf('UID', module)
  .addDecorator(
    (getStory) => (<StoreProvider store={store}>
      {getStory()}
    </StoreProvider>
    )
  )
  .addDecorator(
    (getStory) => (
      <UIDReset>
          {getStory()}
      </UIDReset>
    )
  )
  .add('uid tests', () =>
    <UIDTests/>)
*/
