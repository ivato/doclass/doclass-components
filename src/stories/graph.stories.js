import React from 'react'
import { storiesOf } from '@storybook/react'
import GTPTreeMap from '../presentational/GTPTreeMap'

import { 
  createStore, 
  StoreProvider,   
} from 'easy-peasy'
import { storeModel } from '../store'
const store = createStore(
  storeModel,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

const data_gtp_tm = {
  yh_yc: 218,
  nh_nc: 37,
  uh_uc: 130,
  yh_nc: 33,
  yh_uc: 27,
  nh_yc: 8,
  nh_uc: 3,
  uh_yc: 32,
  uh_nc: 5
}
storiesOf('Graphs', module)
  .addDecorator(
    (getStory) => (<StoreProvider store={store}>
      {getStory()}
    </StoreProvider>
    )
  )
  .add('Ground Truth / Predition relation', () => 
    <GTPTreeMap 
      data={data_gtp_tm}    
    />)
