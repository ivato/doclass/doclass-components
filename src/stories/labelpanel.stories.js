import React from 'react'
import { storiesOf } from '@storybook/react'
import LabelPanelContainer from '../containers/LabelPanelContainer'
import LabelPanel from '../presentational/LabelPanel'
import { DocumentLabelPanel } from '../models/document'

import { 
  createStore, 
  StoreProvider,   
} from 'easy-peasy'
import { storeModel } from '../store'
const store = createStore(
  storeModel,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

storiesOf('LabelPanelContainer', module)
  .addDecorator(
    (getStory) => (<StoreProvider store={store}>
      {getStory()}
    </StoreProvider>
    )
  )
  .add('all labelset deletable', () => <LabelPanelContainer
    title='All labelsets'
    clickable='false'
    deletable='true'
    endpoint='http://doclass.localhost/labelset'
    initial_data='all'
    context='LabelPanelContainer'
  />)
  .add('all labelset clickable', () => <LabelPanelContainer
    title='All labelsets'
    clickable='true'
    deletable='false'
    endpoint='http://doclass.localhost/labelset'
    initial_data='all'
    context='LabelPanelContainer'
  />)
  .add('all labels from 20_newsgroups labelset', () => <LabelPanelContainer
    title='All labels from 20 newsgroups labelset'
    clickable='false'
    deletable='true'
    endpoint='http://doclass.localhost/labelset/1/labels'
    initial_data='all'
    context='LabelPanelContainer'
  />)
storiesOf('LabelPanel', module)
  .addDecorator(
    (getStory) => (<StoreProvider store={store}>
      {getStory()}
    </StoreProvider>
    )
  )
  .add('stateless', () => <LabelPanel
        labels={[ {_id: 1, full_name: 'one', description: 'one label'}, {_id: 2, full_name: 'two', description: 'two label'}]}
        clickable='true'
        deletable='false'
        title='Stateless Labels'
    />)
storiesOf('DocumentLabelPanel', module)
  .addDecorator(
    (getStory) => (<StoreProvider store={store}>
      {getStory()}
    </StoreProvider>
    )
  )
  .add('all labels from document of id=1', () => <DocumentLabelPanel
    title='All labels from document of id=1'
    clickable='false'
    deletable='true'
    docId='1'
    context='LabelPanelContainer'
    endpoint='http://doclass.localhost/document/1/labels'
  />)
