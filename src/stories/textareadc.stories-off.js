import React from 'react'

import { storiesOf } from '@storybook/react'

import TextAreaDC from '../presentational/TextArea'

storiesOf('TextArea', module)
  .add('document with static content', () => <TextAreaDC
    // endpoint='http://doclass.localhost/document/content'
    // id='1'
    content='testing content'
  />)
