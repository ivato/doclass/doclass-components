import React from 'react'

import { storiesOf } from '@storybook/react'
import LabelsetPanelContainer from '../containers/LabelsetPanelContainer'
import { 
  createStore, 
  StoreProvider,   
} from 'easy-peasy'
import { storeModel } from '../store'
const store = createStore(
  storeModel,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

storiesOf('LabelsetPanelContainer', module)
  .addDecorator(
    (getStory) => (<StoreProvider store={store}>
      {getStory()}
    </StoreProvider>
    )
  )
  /*.addDecorator(
    (getStory) => (<useItemsContext.Provider>{getStory()}</useItemsContext.Provider>)
  )
  .addDecorator(
    (getStory) => (<useLabelsContext.Provider>{getStory()}</useLabelsContext.Provider>)
  )
  .addDecorator(
    (getStory) => (<useSelectedValueContext.Provider>{getStory()}</useSelectedValueContext.Provider>)
  )
  .addDecorator(
    (getStory) => (<useEndpointContext.Provider>{getStory()}</useEndpointContext.Provider>))
  .addDecorator(
    (getStory) => (<useContextBinding.Provider>{getStory()}</useContextBinding.Provider>))  
  .addDecorator(
    (getStory) => (<useContextPubSub.Provider>{getStory()}</useContextPubSub.Provider>))*/    
  .add('labelset select', () =>
    <LabelsetPanelContainer
      />)
  .add('labelset filter: excluded (meta and computer)', () =>
    <LabelsetPanelContainer
      excludedValues={['computer', 'meta']}
      excludedField='properties' />)
