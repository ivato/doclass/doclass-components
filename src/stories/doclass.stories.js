import React, {useEffect} from 'react'
import { storiesOf } from '@storybook/react'
import DocViewerContainer from '../containers/DocViewerContainer'
import DocBrowserContainer from '../containers/DocBrowserContainer'
import DocExplorerContainer from '../containers/DocExplorerContainer'
import { 
  createStore, 
  StoreProvider,   
} from 'easy-peasy'
import { storeModel } from '../store'
const store = createStore(
  storeModel,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

storiesOf('Document Classification', module)
  .addDecorator(
    (getStory) => (<StoreProvider store={store}>
      {getStory()}
    </StoreProvider>
    )
  )
  .add('document viewer', () =>
    <DocViewerContainer
      showSelectDoc='true'
      debugComponentTree='false'
    />)
  .add('document browser', () =>
    <DocBrowserContainer
      showSelectDoc='true'
    />)
  .add('document explorer', () => 
    <DocExplorerContainer
      debugComponentTree='false'
    />)