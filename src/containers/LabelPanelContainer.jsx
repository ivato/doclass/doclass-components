import React, { useEffect, useState } from 'react'
import LabelPanel from '../presentational/LabelPanel'
import { 
  useStoreState,
  useStoreActions 
} from 'easy-peasy'

export function LabelPanelContainer(props) {
  const name = 'LabelPanelContainer'  
  const { parent } = props
  const registerComponent = useStoreActions(actions => actions.registerComponent);
  const unRegisterComponent = useStoreActions(actions => actions.unRegisterComponent);
  const updateLabels = useStoreActions(actions => actions.updateLabels);
  const setEndpoint = useStoreActions(actions => actions.setEndpoint);
  const clearEvent = useStoreActions(actions => actions.clearEvent)
  const labels = useStoreState(state => state.events.labels);
  const uid = props.uid ? props.uid : '1-'+name
  const uid_prefix = uid.substring(0,uid.lastIndexOf('-'))
  const lpuid = uid_prefix+'.1-LabelPanel' 

  useEffect(()=>{
    registerComponent({ uid, parent, name })
    return () => { 
      unRegisterComponent({uid,name})
    }
  },[]) 

  const {
    clickable,
    deletable,
    title,
    excludeLabels,
    endpoint,
    options,
    handleDelete,
    handleClick
  } = props

  useEffect(() => {
    if ( typeof endpoint !== 'undefined' && endpoint ) {
      updateLabels({uid, endpoint})
      return () => { 
        clearEvent({uid, name: 'endpoint'})
        clearEvent({uid, name: 'labels'})
      }   
    }
  }, [endpoint])

  return (
    <LabelPanel
      labels={labels[uid]}
      clickable={clickable}
      deletable={deletable}
      title={title}
      options={options}
      handleDelete={handleDelete}
      handleClick={handleClick}
      parent={uid}
      uid={lpuid}
    />
  )
}

export default LabelPanelContainer