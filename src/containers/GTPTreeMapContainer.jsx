import React, { useEffect, useState } from 'react'
import GTPTreeMap from '../presentational/GTPTreeMap'
import { 
  useStoreState,
  useStoreActions 
} from 'easy-peasy'

function GTPTreeMapContainer (props) {
  const name = 'GTPTreeMapContainer'  
  const updateItems = useStoreActions(actions => actions.updateItems);
  const {
    parent
  } = props

  const registerComponent = useStoreActions(actions => actions.registerComponent);
  const unRegisterComponent = useStoreActions(actions => actions.unRegisterComponent);
  const clearEvent = useStoreActions(actions => actions.clearEvent)
  const uid = props.uid ? props.uid : '1-'+name
  const uid_prefix = uid.substring(0,uid.lastIndexOf('-'))
  const gtptmuid = uid_prefix+'.1-GTPTreeMap'
  const items = useStoreState( state => state.events.items )

  useEffect(()=>{
    registerComponent({ uid, parent, name })
    return () => { unRegisterComponent({uid,name}) }
  },[]) 

  useEffect(() => {
    endpoint && updateItems({ uid, endpoint })
    return () => { 
      clearEvent({uid, name: 'items'})
    }
  }, [endpoint])

  return (
    <GTPTreeMapContainer
      title={title}
      parent={uid}
      uid={gtptmuid}
    />
  )
}

export default SelectItemContainer
