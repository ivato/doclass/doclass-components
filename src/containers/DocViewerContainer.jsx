import React, { useState, useEffect } from 'react'
import TextArea from '../presentational/TextArea'
import styles from '../styles/styles'
import { withStyles } from '@material-ui/core/styles'
import LabelsetPanelContainer from './LabelsetPanelContainer'
import { DocumentLabelPanel } from '../models/document'
import SelectDocumentContainer from '../containers/SelectDocumentContainer'
import AlertDialog from '../presentational/AlertDialog'

import { 
  useStoreState,
  useStoreActions 
} from 'easy-peasy'

function DocViewerContainer(props) {
  const name = 'DocViewerContainer'  
  const { 
    parent,
    options,
    showSelectDoc,
    docSelectUid
  } = props
  const registerComponent = useStoreActions(actions => actions.registerComponent)
  const unRegisterComponent = useStoreActions(actions => actions.unRegisterComponent)
  const uid = props.uid ? props.uid : '1-'+name
  const uid_prefix = uid.substring(0,uid.lastIndexOf('-'))

  useEffect(()=>{
    registerComponent({ uid, parent, name })
    return () => { unRegisterComponent({ uid, name }) }
  },[]) 

  let sdcuid = uid_prefix+'.1-SelectDocumentContainer'
  if ( docSelectUid ) {
    sdcuid = docSelectUid
  }

  const lpcuid = uid_prefix+'.2-LabelsetPanelContainer' 
  const lpcuid_lp = uid_prefix+'.2.2.1-LabelPanel' 
  const dlpuid = uid_prefix+'.3-DocumentLabelPanel' 
  const dlpuid_lpc = uid_prefix+'.3.1-LabelPanelContainer' 
  const dlpuid_lp = uid_prefix+'.3.1.1-LabelPanel' 
  const tauid = uid_prefix+'.4-TextArea' 
  const aduid = uid_prefix+'.5-AlertDialog' 

  const clicked = useStoreState(state => state.events.clicked_labels)
  const deleted = useStoreState(state => state.events.deleted_labels)
  const selected = useStoreState(state => state.events.selected)
  const labels = useStoreState(state => state.events.labels)
  const contents = useStoreState(state => state.events.contents)

  const updateDocumentLabels = useStoreActions(actions => actions.updateDocumentLabels)
  const updateContent = useStoreActions(actions => actions.updateContent)
  const updateHiddenLabels = useStoreActions(actions => actions.updateHiddenLabels)
  const applyLabel = useStoreActions(actions => actions.applyLabel)
  const removeLabel = useStoreActions(actions => actions.removeLabel)
  const updateDocumentId = useStoreActions(actions => actions.updateDocumentId)

  useEffect(
    () => {
      if ( typeof selected[sdcuid] !== 'undefined' ) {
        const doc_id = selected[sdcuid]
        let uid = dlpuid_lpc
        const hidden_uid = lpcuid_lp
        updateDocumentLabels({uid, hidden_uid, doc_id})
        updateContent({uid: tauid, doc_id})
        updateDocumentId({ uid: dlpuid, doc_id})
    } 
  },[selected[sdcuid]])

  useEffect(() => {
    if (typeof selected[sdcuid] !== 'undefined') {
      const doc_id = selected[sdcuid]
      const origin_uid = lpcuid_lp
      const target_uid = dlpuid_lpc
      const label_id = clicked[lpcuid_lp]._id
      const alert_uid = aduid
      applyLabel({doc_id, origin_uid, target_uid, label_id, alert_uid})  
    }
  },[clicked[lpcuid_lp]])

  useEffect(() => {
    if (typeof deleted[dlpuid_lp] !== 'undefined') {
      const doc_id = selected[sdcuid]
      const uid = dlpuid_lpc
      const label_id = deleted[dlpuid_lp]._id
      const hidden_uid = lpcuid_lp
      removeLabel({doc_id, uid, label_id, hidden_uid})  
    }    
  },[deleted[dlpuid_lp]])

  const alias = {
    'ac.h.y': 'ac.h.YES',
    'ac.h.n': 'ac.h.NO',
    'ac.h.u': 'ac.h.UNCERTAIN',
    'ac.c.y': 'ac.c.YES',
    'ac.c.n': 'ac.c.NO',
    'ac.c.u': 'ac.c.UNCERTAIN'
  }
  const color = {
    'ac.h.y': '#00FF00',
    'ac.h.n': '#FF0000',
    'ac.h.u': '#FFFF00',
    'ac.c.y': '#00FF00',
    'ac.c.n': '#FF0000',
    'ac.c.u': '#FFFF00'
  }
  const deletable = {
    'false': 'ac.c.*'
  }
  const newOptions = {
    ...options,
    alias,
    color,
    deletable
  }
  
  return (
    <>
      <AlertDialog
        parent={uid}
        uid={aduid}
        title="Error: could not aplly label"
        content="Probably because there is a label conflict"
      />
      {(showSelectDoc && showSelectDoc === 'true')
        ? <SelectDocumentContainer
            parent={uid}
            uid={sdcuid}
          />
        : null
      }
      <LabelsetPanelContainer
        excludedValues={['computer', 'meta']}
        excludedField='properties'
        clickable={true}
        parent={uid}
        uid={lpcuid}
      />
      <DocumentLabelPanel
        title='Current labels'
        deletable='true'
        clickable={true}
        options={newOptions}
        parent={uid}
        uid={dlpuid}
      />
      <TextArea
        content={contents[tauid]}
        parent={uid}
        uid={tauid}
      />
    </>
  )
}

export default withStyles(styles())(DocViewerContainer)