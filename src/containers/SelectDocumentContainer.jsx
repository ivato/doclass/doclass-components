import React, {useEffect, useState } from 'react'
import ColllectionSelect  from '../models/collection'
import RepositorySelect from '../models/repository'
import { DocumentSelect } from '../models/document'
import Paper from '@material-ui/core/Paper'
import styles from '../styles/styles'
import { withStyles } from '@material-ui/core/styles'
import { useStoreActions, useStoreState } from 'easy-peasy'
import config from '../config'
import Grid from "@material-ui/core/Grid";
function SelectDocumentContainer(props) {
  const name = 'SelectDocumentContainer'
  const { 
    parent
  } = props
  const selected = useStoreState(state => state.events.selected)
  const endpoint = useStoreState(state => state.events.endpoint)

  const registerComponent = useStoreActions(actions => actions.registerComponent);
  const unRegisterComponent = useStoreActions(actions => actions.unRegisterComponent);
  const clearEvent = useStoreActions(actions => actions.clearEvent);
  const setEndpoint = useStoreActions(actions => actions.setEndpoint);
  const updateSelected = useStoreActions(actions => actions.updateSelected)

  const uid = props.uid ? props.uid : '1-'+name
  const uid_prefix = uid.substring(0,uid.lastIndexOf('-'))
  const csuid = uid_prefix+'.1-CollectionSelect' 
  const si_csuid = uid_prefix+'.1.1.1-SelectItem'
  const rsuid = uid_prefix+'.2-RepositorySelect' 
  const si_rsuid = uid_prefix+'.2.1.1-SelectItem'
  const dsuid = uid_prefix+'.3-DocumentSelect' 
  const si_dsuid = uid_prefix+'.3.1.1-SelectItem'

  useEffect(()=> {
    registerComponent({ uid, parent, name })
    return () => { 
      unRegisterComponent({uid,name}) 
    }
  },[])

  useEffect(()=>{
      if ( selected[si_csuid] ) {
        const collection_id = selected[si_csuid]
        const endpoint = config.getEndpoint(
          'collection',
          'repo_list',
          {collection_id}
        )
        updateSelected( { uid: csuid, data: selected[si_csuid] })
        setEndpoint({uid: rsuid, endpoint})
        return () => { 
          clearEvent({uid: rsuid, name: 'endpoint'})
          clearEvent({uid: csuid, name: 'selected'})
        }
      }
    },[selected[si_csuid]]
  )

  useEffect(() => {
      if ( selected[si_rsuid] ) {
        const repo_id = selected[si_rsuid]
        const endpoint = config.getEndpoint(
          'document',
          'docs_repo',
          {repo_id}
        )
        setEndpoint({uid: dsuid, endpoint})
        updateSelected( { uid: rsuid, data: selected[si_rsuid] })
        return () => { 
          clearEvent({uid: dsuid, name: 'endpoint'}) 
          clearEvent({uid: rsuid, name: 'selected'}) 
        }        
      }
    },[selected[si_rsuid]]
  )
  
  useEffect(
    () => {
      if ( selected[si_dsuid] ) {
        const data = selected[si_dsuid]
        updateSelected( { uid: dsuid, data: selected[si_dsuid] })
        updateSelected( { uid, data })
        return () => { 
          clearEvent({uid, name: 'selected'}) 
          clearEvent({uid: dsuid, name: 'selected'}) 
        }  
      }
    },
    [selected[si_dsuid]]
  )

  return (
    <Grid container 
      direction="row" 
      justify="flex-start" 
      alignItems="flex-start"
      spacing={2}
      >
      <Grid item xs={4}>
        <ColllectionSelect
          parent={uid}
          uid={csuid}
        />
      </Grid>
      <Grid item xs={4}>
        <RepositorySelect 
          parent={uid}
          uid={rsuid}      
          endpoint={endpoint[rsuid]}
        />
      </Grid>
      <Grid item xs={4}>
        <DocumentSelect
          parent={uid}
          uid={dsuid}
          endpoint={endpoint[dsuid]}
        />
      </Grid>
    </Grid>
  )
}
export default (withStyles(styles())(SelectDocumentContainer))