import React, { useEffect, useState } from 'react'
import SelectItem from '../presentational/SelectItem'
import { 
  useStoreState,
  useStoreActions 
} from 'easy-peasy'

function SelectItemContainer (props) {
  const name = 'SelectItemContainer'  
  const updateItems = useStoreActions(actions => actions.updateItems);
  const {
    showid,
    endpoint,
    excludedField,
    excludedValues,
    key_,
    field,
    title,
    parent
  } = props

  const registerComponent = useStoreActions(actions => actions.registerComponent);
  const unRegisterComponent = useStoreActions(actions => actions.unRegisterComponent);
  const clearEvent = useStoreActions(actions => actions.clearEvent)
  const uid = props.uid ? props.uid : '1-'+name
  const uid_prefix = uid.substring(0,uid.lastIndexOf('-'))
  const siuid = uid_prefix+'.1-SelectItem'
  const items = useStoreState( state => state.events.items )

  useEffect(()=>{
    registerComponent({ uid, parent, name })
    return () => { unRegisterComponent({uid,name}) }
  },[]) 


  useEffect(() => {
    endpoint && updateItems({ uid, endpoint, excludedField, excludedValues })
    return () => { 
      clearEvent({uid, name: 'items'})
    }
  }, [endpoint])

  return (
    <SelectItem
      title={title}
      items={
        items && items[uid] ? items[uid] : []
      }
      key_={key_}
      showid={showid}
      field={field}
      parent={uid}
      uid={siuid}
    />
  )
}

export default SelectItemContainer
