import React, { useState, useEffect } from 'react'
import MaterialTable from 'material-table'
import 'typeface-roboto'
import axios from 'axios'
import {
  Search,
  SaveAlt,
  ChevronLeft,
  ChevronRight,
  FirstPage,
  LastPage,
  FilterList,
  Remove,
  Clear,
  Delete,
  Add,
  Edit  
} from '@material-ui/icons'

function TableContainer (props) {
  const [data, setData] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const {
    editableOption,
    endpoint,
    entity,
    options
  } = props

  useEffect(() => {
    if (endpoint) {
      console.log("TCL - TableContainer: endpoint", endpoint)
      setIsLoading(true)
      axios.get(endpoint)
      .then(res => {
        setData(res.data)
        setIsLoading(false)
      })
    }
  }, [endpoint])

  function delEntity (id) {
    const url = endpoint + '/' + id
    axios.delete(url)
      .then(res => {
        if (res.data === id) {
          window.alert(entity + ' ' + id + ' deleted')
        }
      })
  }

  function updateEntity (newData) {
    axios
      .put(endpoint, newData)
      .then(res => {
        window.alert(entity + ' ' + newData._id + ' updated')
      })
      .catch(error => {
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data)
          console.log(error.response.status)
          console.log(error.response.headers)
        } else if (error.request) {
          // The request was made but no response was received
          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
          // http.ClientRequest in node.js
          console.log(error.request)
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log('Error', error.message)
        }
        console.log(error.config)
      })
  }
  /*
  function addEntity (newData) {
    axios.post(
      // this.props.endpoint,
      this.props.endpoint,
      newData
    )
      .then(res => {
        window.alert(this.props.entity + ' ' + newData._id + ' added')
      })
      .catch(error => {
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data)
          console.log(error.response.status)
          console.log(error.response.headers)
        } else if (error.request) {
          // The request was made but no response was received
          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
          // http.ClientRequest in node.js
          console.log(error.request)
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log('Error', error.message)
        }
        console.log(error.config)
      })
  }
  */
  // render () {
  const editableTrue = {
    // onRowAdd não está funcionando :-\
    /* onRowAdd: newData =>
                      new Promise(( resolve, reject ) => {
                          setTimeout(() => {
                              {
                                  this.addEntity(newData)
                                  const data = this.state.data
                                  data.push(newData)
                                  this.setState( { data }, () => resolve())
                              }
                              resolve()
                          }, 1000)
                      }), */
    onRowUpdate: (newData, oldData) =>
      new Promise((resolve, reject) => {
        setTimeout(() => {
          {
            updateEntity(newData)
            const index = data.indexOf(oldData)
            data[index] = newData
            setData({ data }, () => resolve())
          }
          resolve()
        }, 1000)
      }),
    onRowDelete: oldData =>
      new Promise((resolve, reject) => {
        setTimeout(() => {
          {
            delEntity(oldData._id)
            const index = data.indexOf(oldData)
            data.splice(index, 1)
            setData({ data }, () => resolve())
          }
          resolve()
        }, 1000)
      })
  }
  return (
    <MaterialTable
      icons={{
        DetailPanel: ChevronRight,
        Export: SaveAlt,
        Filter: FilterList,
        FirstPage: FirstPage,
        LastPage: LastPage,
        NextPage: ChevronRight,
        PreviousPage: ChevronLeft,
        Search: Search,
        ThirdStateCheck: Remove,
        ResetSearch: Clear,
        Delete: () => <Delete />,
        Edit: Edit
      }}
      title={props.title}
      data={data}
      columns={props.columns}
      editable={editableOption === 'true' ? editableTrue : {}}
      options={options}
      isLoading={isLoading}
    />
  )
}

export default TableContainer
