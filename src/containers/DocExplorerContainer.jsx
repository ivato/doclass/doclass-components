import React, { useEffect } from 'react'
import styles from '../styles/styles'
import { withStyles } from '@material-ui/core/styles'
import DocBrowserContainer from './DocBrowserContainer'
import GTPTreeMap from '../presentational/GTPTreeMap'
import DocViewerContainer from './DocViewerContainer'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import * as Spaces from 'react-spaces';

import { 
  useStoreState,
  useStoreActions 
} from 'easy-peasy'

function DocExplorerContainer (props) {
  const name = 'DocExplorerContainer'  
  const uid = props.uid ? props.uid : '1-'+name
  const uid_prefix = uid.substring(0,uid.lastIndexOf('-'))
  const { parent } = props
  const registerComponent = useStoreActions(actions => actions.registerComponent)
  const unRegisterComponent = useStoreActions(actions => actions.unRegisterComponent)

  useEffect(()=>{
    registerComponent({ uid, parent, name })
    return () => { unRegisterComponent({ uid, name }) }
  },[]) 


  const gtpuid = uid_prefix+'.1-GroundTruthPredictionTM'
  const dbcuid = uid_prefix+'.2-DocBrowserContainer'
  const dbcuid_sdc = uid_prefix+'.2.1-SelectDocumentContainer'
  const dvcuid = uid_prefix+'.3-DocViewerContainer'

  return (
    <Spaces.ViewPort>
      <Spaces.LeftResizable size="50%">
          <Spaces.TopResizable size="40%">
            <GTPTreeMap
              uid={gtpuid}
              parent={uid}
            />
          </Spaces.TopResizable>
          <Spaces.Fill>
            <DocBrowserContainer
                parent={uid}
                uid={dbcuid}
              />
          </Spaces.Fill>
      </Spaces.LeftResizable>
      <Spaces.Fill>
          <DocViewerContainer
            parent={uid}
            uid={dvcuid}
            docSelectUid={dbcuid_sdc}
          />
      </Spaces.Fill>
    </Spaces.ViewPort>
  )
}

export default withStyles(styles())(DocExplorerContainer)