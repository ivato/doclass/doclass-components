import React, {useEffect, useState} from 'react'
import styles from '../styles/styles'
import Paper from '@material-ui/core/Paper'
import { withStyles } from '@material-ui/core/styles'
import SelectDocumentContainer from './SelectDocumentContainer'
import TableContainer from './TableContainer'
import { 
  useStoreState,
  useStoreActions 
} from 'easy-peasy'
import config from '../config'

function DocBrowserContainer (props) {
  const name = 'DocBrowserContainer'  
  const { parent, showSelectDoc, docSelectUid } = props
  const registerComponent = useStoreActions(actions => actions.registerComponent);
  const unRegisterComponent = useStoreActions(actions => actions.unRegisterComponent)
  const uid = props.uid ? props.uid : '1-'+name
  const uid_prefix = uid.substring(0,uid.lastIndexOf('-'))
  const selected = useStoreState(state => state.events.selected)

  useEffect(()=>{
    registerComponent({ uid, parent, name })
    return () => { unRegisterComponent({ uid, name }) }
  },[]) 

  const sdcuid = uid_prefix+'.1-SelectDocumentContainer'
  if ( docSelectUid ) {
    sdcuid = docSelectUid
  }
  const sdcuid_prefix = sdcuid.substring(0,sdcuid.lastIndexOf('-'))
  const csuid = sdcuid_prefix+'.1-CollectionSelect'
  const rsuid = sdcuid_prefix+'.2-RepositorySelect' 
  const dsuid = sdcuid_prefix+'.3-DocumentSelect' 
  const [endpoint, setEndpoint] = useState('')

  useEffect(()=>{
      if ( selected[csuid] ) {
        const collection_id = selected[csuid]
        const endpoint1 = config.getEndpoint(
          'document',
          'get_classifications',
          {entity: 'collection', id: collection_id}
        )
        console.log('endpoint1: '+endpoint1)
        setEndpoint(endpoint1)
      }
    },[selected[csuid]]
  )

  useEffect(()=>{
    if ( selected[rsuid] ) {
      const repo_id = selected[rsuid]
      const endpoint = config.getEndpoint(
        'document',
        'get_classifications',
        {entity: 'repo', id: repo_id}
      )
      setEndpoint(endpoint)
    }},[selected[rsuid]]
  )

  useEffect(()=>{
    if ( selected[dsuid] ) {
      const doc_id = selected[dsuid]
      const endpoint = config.getEndpoint(
        'document',
        'get_classifications',
        {entity: 'doc', id: doc_id}
      )
      setEndpoint(endpoint)
    }},[selected[dsuid]]
  )

  const tcuid = uid_prefix+'.2-TableContainer'

  function renderLabel (id) {
    const x = {
      4: ['YC', '#00FF00'],
      5: ['NC', '#FF0000'],
      8: ['UC', '#FFFF00'],
      2: ['YH', '#00FF00'],
      3: ['NH', '#FF0000'],
      7: ['UH', '#FFFF00']
    }
    const v = x[id] ? x[id] : ''
    return (
      <div style={{ background: v[1] }}>{v[0]}</div>
    )
  }

  function renderValue (value) {
    const Rainbow = require('rainbowvis.js')
    const r = new Rainbow()
    r.setNumberRange(-1, 1)
    r.setSpectrum('FF0000', 'FFFF00', '00FF00')
    const color = '#' + r.colorAt(value)
    return (
      <div style={{ background: color }}>{value}</div>
    )
  }

  return (
    <>
      {(showSelectDoc && showSelectDoc === 'true')?
        <SelectDocumentContainer
        parent={uid}
        uid={sdcuid}
        />
        : null
      }
      <Paper>
        <TableContainer
          editableOption='false'
          parent={uid}
          uid={tcuid}
          endpoint={endpoint}
          title='Document Classification'
          entity='document'
          columns={[
            {
              field: '_id',
              title: 'Id',
              editable: 'never'
            },
            {
              field: 'filename',
              title: 'Filename',
              editable: 'never'
            },
            {
              field: 'alg_value',
              title: 'Algorithm Value',
              editable: 'never',
              render: rowData => renderValue(rowData.alg_value)
            },
            {
              field: 'alg_label',
              title: 'Algorithm Label',
              editable: 'never',
              render: rowData => renderLabel(rowData.alg_label)
            },
            {
              field: 'hum_value',
              title: 'Human Value',
              editable: 'never',
              render: rowData => renderValue(rowData.hum_value)
            },
            {
              field: 'hum_label',
              title: 'Human Label',
              editable: 'never',
              render: rowData => renderLabel(rowData.hum_label)
            }
          ]}
          actions={[]}
          options={{
            selection: true,
            filtering: true
          }}
        />
      </Paper>
    </>
  )
}

export default withStyles(styles())(DocBrowserContainer)