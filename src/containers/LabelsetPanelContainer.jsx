import React, { useEffect, useState } from 'react'
import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import styles from '../styles/styles'
import LabelPanelContainer from './LabelPanelContainer'
import LabelsetSelect from '../models/labelset'
import { 
  useStoreState,
  useStoreActions 
} from 'easy-peasy'
import config from '../config'

function LabelsetPanelContainer(props) {
  const name = 'LabelsetPanelContainer'  
  const { parent } = props

  const registerComponent = useStoreActions(actions => actions.registerComponent);
  const unRegisterComponent = useStoreActions(actions => actions.unRegisterComponent);
  const uid = props.uid ? props.uid : '1-'+name
  const uid_prefix = uid.substring(0,uid.lastIndexOf('-'))  
  const lsuid = uid_prefix+'.1-LabelsetSelect'
  const si_lsuid = uid_prefix+'.1.1.1-SelectItem'
  const lpcuid = uid_prefix+'.2-LabelPanelContainer'

  useEffect(()=>{
    registerComponent({ uid, parent, name })
    return () => { unRegisterComponent({uid,name}) }
  },[]) 

  const selected = useStoreState(state => state.events.selected);
  const endpoint = useStoreState(state => state.events.endpoint);
  const setEndpoint = useStoreActions(actions => actions.setEndpoint);
  const clearEvent = useStoreActions(actions => actions.clearEvent)

  useEffect(() => {
      if ( typeof selected[si_lsuid] !== 'undefined') {
        const labelset_id = selected[si_lsuid]
        if ( typeof labelset_id !== 'undefined' && labelset_id.length !== 0) {
          const endpoint = config.getEndpoint(
            'labelset',
            'get_labels',
            {labelset_id}
          )
          setEndpoint({uid: lpcuid, endpoint})  
        }
      }
      return () => { 
        clearEvent({uid: lpcuid, name: 'endpoint'})
      }  
    }
    ,[selected[si_lsuid]]
  )
  
  const {
    excludedValues,
    excludedField,
    classes,
    title,
    excludeLabels,
    clickable
  } = props

  return (
    <Grid 
      container 
      direction="row" 
      justify="flex-start" 
      alignItems="flex-start"
      spacing={2}
      className={classes.root}>
      <Grid item xs={6}>
        <LabelsetSelect
          excludedField={excludedField}
          excludedValues={excludedValues}
          uid={lsuid}
          parent={uid}
        />
      </Grid>
      <Grid item xs={6}>
        <LabelPanelContainer
          title={title}
          clickable={clickable}
          deletable='false'
          options={{
            alias: {
              'ac.h.y': 'YES',
              'ac.h.n': 'NO',
              'ac.h.u': 'UNCERTAIN'
            },
            color: {
              'ac.h.y': '#00FF00',
              'ac.h.n': '#FF0000',
              'ac.h.u': '#FFFF00'
            }
          }}
          excludeLabels={excludeLabels}
          endpoint={endpoint[lpcuid]}
          uid={lpcuid}
          parent={uid}
        />
      </Grid>
    </Grid>
  )
}

export default withStyles(styles())(LabelsetPanelContainer)