import { configure, addParameters } from '@storybook/react'

import '../src/index.css';

addParameters({
	options: {
		showAddonsPanel: false,
	},
});

const req = require.context('../src/stories', true, /.stories.js$/);

function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
